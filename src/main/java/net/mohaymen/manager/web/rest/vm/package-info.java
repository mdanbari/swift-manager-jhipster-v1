/**
 * View Models used by Spring MVC REST controllers.
 */
package net.mohaymen.manager.web.rest.vm;
