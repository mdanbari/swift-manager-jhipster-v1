/**
 * Data Access Objects used by WebSocket services.
 */
package net.mohaymen.manager.web.websocket.dto;
