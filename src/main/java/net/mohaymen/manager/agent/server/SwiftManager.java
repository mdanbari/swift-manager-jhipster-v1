//package net.mohaymen.manager.agent.server;
//
//import java.io.BufferedReader;
//import java.io.InputStreamReader;
//
//import javax.annotation.PostConstruct;
//
//import org.springframework.stereotype.Component;
//
//import io.netty.bootstrap.ServerBootstrap;
//import io.netty.channel.EventLoopGroup;
//import io.netty.channel.nio.NioEventLoopGroup;
//import io.netty.channel.socket.nio.NioServerSocketChannel;
//
//@Component
//public class SwiftManager {
//	
//	
//	private final int port;
//
//	public SwiftManager(int port) {
//		this.port = port;
//	}
//
//	@PostConstruct
//	public void startManager() throws Exception {
//		
//			new SwiftManager(8000).run();
//	}
//	
//	
//	
//	public void run() throws Exception
//	{
//		EventLoopGroup baseGroup = new NioEventLoopGroup();
//		EventLoopGroup workerGroup = new NioEventLoopGroup();
//		try {
//			
//			ServerBootstrap bootstrap = new ServerBootstrap()
//					.group(baseGroup,workerGroup)
//					.channel(NioServerSocketChannel.class)
//					.childHandler(new SwiftManagerInitializer());
//			bootstrap.bind(port).sync();//.channel().closeFuture().sync();
//			
////			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
////
////			SwiftManagerChannelInitializer swiftManagerChannelInitializer = new SwiftManagerChannelInitializer();
////            while (true) {
////                String cmd = in.readLine();                
////                swiftManagerChannelInitializer.writeCmdToChannel(cmd, "192.168.70.180");
////            }
//		}
//		finally {
//			baseGroup.shutdownGracefully();
//			workerGroup.shutdownGracefully();
//		}
//	}
//
//}
