//package net.mohaymen.manager.agent.server;
//
//import java.net.SocketAddress;
//import java.util.HashMap;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import io.netty.channel.Channel;
//import io.netty.channel.ChannelHandlerContext;
//import io.netty.channel.ChannelId;
//import io.netty.channel.ChannelInboundHandlerAdapter;
//import io.netty.channel.group.ChannelGroup;
//import io.netty.channel.group.DefaultChannelGroup;
//import io.netty.util.concurrent.GlobalEventExecutor;
//
//public class SwiftManagerChannelInitializer extends ChannelInboundHandlerAdapter {
//
//	private static final ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
//	private static HashMap<String, ChannelId> channelIdMap = new HashMap<>();
//	ObjectMapper mapper = new ObjectMapper();
//
//	@Override
//	public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
//		Channel incoming = ctx.channel();
//		ChannelId channelid = incoming.id();
//		SocketAddress remoteAddress = incoming.remoteAddress();
//		channelIdMap.put(remoteAddress.toString().replaceFirst("/", "").split(":")[0], channelid);
//		SwiftCommand command = new SwiftCommand();
//		command.setCommandName("[SERVER] - " + "WELCOME TO THIS SIMPLE AGENT APP!\r\n");
//		command.setArg1("arg1");
//		command.setArg2("arg2");
//		command.setArg3("arg3");			
//		String jsonInString = mapper.writeValueAsString(command);
//
//		incoming.writeAndFlush( jsonInString + "\r\n");
//		
//		System.out.println("[CLIENT] - " + incoming.remoteAddress() + " has joined\r\n");
//		
//
//		// for (Channel channel : channels) {
//		// channel.writeAndFlush("[SERVER] - " + incoming.remoteAddress() + "
//		// has joined\r\n");
//		// }
//		channels.add(incoming);
//	}
//
//	@Override
//	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
//		Channel incoming = ctx.channel();
//		for (Channel channel : channels) {
//			channel.writeAndFlush("[SERVER] - " + incoming.remoteAddress() + " has left\r\n");
//		}
//		channels.remove(incoming);
//		SocketAddress remoteAddress = incoming.remoteAddress();
//		channelIdMap.remove(remoteAddress.toString().replaceFirst("/", "").split(":")[0]);
//		
//		System.out.println("[CLIENT] - " + incoming.remoteAddress() + " has left\r\n");
//	}
//
//	@Override
//	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
//		System.out.println("[CLIENT] - " + msg + "\r\n");
//	}
//	// @Override
//	// protected void channelRead0(ChannelHandlerContext channelHandlerContext,
//	// String msg) throws Exception {
//	// System.out.println(msg);
//	//
//	//// Channel incoming = channelHandlerContext.channel();
//	//// for (Channel channel : channels) {
//	//// if (channel != incoming) {
//	//// channel.writeAndFlush("[" + incoming.remoteAddress() + "] " + msg +
//	// "\r\n");
//	//// } else {
//	//// channel.writeAndFlush("[you] " + msg + "\r\n");
//	//// }
//	//// }
//	//
//	// }
//
//	public void writeCmdToChannel(String cmd, String remoteAddrs) throws JsonProcessingException {
//
//		ChannelId channelId = channelIdMap.get(remoteAddrs);
//		if(channelId != null)
//		{
//			Channel currentChannel = channels.find(channelId);
//			SwiftCommand command = new SwiftCommand();
//			command.setCommandName(cmd);
//			command.setArg1("arg1");
//			command.setArg2("arg2");
//			command.setArg3("arg3");			
//			String jsonInString = mapper.writeValueAsString(command);
//			currentChannel.writeAndFlush(jsonInString + "\r\n");
//	
////			for (Channel channel : channels) {
////				channel.write("\n" + "[" + cmd + "]" + "\n");
////			}
//		}
//	}
//
//	@Override
//	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
//		cause.printStackTrace();
//		ctx.close();
//	}
//
//}
